#!/usr/bin/ruby
# coding: utf-8 

Encoding.default_external = "UTF-8"

require 'net/https'
require 'uri'
require 'json'
require 'sqlite3'

module Book

PageSize = 50

# not strict
def self.isIsbn(isbn_str)
  if isbn_str.instance_of?(String) && /^[0-9]+[0-9X]$/ =~ isbn_str &&
     (isbn_str.length == 10 || isbn_str.length == 13)
     true
  else
     false
  end
end

def self.isExist(db,isbn_str)
	sql_str = "select count(isbn) from booklist where isbn='#{isbn_str}';"
	result = db.execute(sql_str)
	if result[0][0] == 0
		false
	else
		true
	end
end

def self.getBookInfo(isbn_str)  
  if isIsbn(isbn_str)
    https = Net::HTTP.new('www.googleapis.com', 443)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    https.verify_depth = 5

    https.start {|w|
      response = w.get('/books/v1/volumes?q=isbn:'+isbn_str)
      json = JSON.parse(response.body)
      if json["totalItems"] == 1
        json["items"][0]["volumeInfo"]
      else
        ({})
      end
    }
  else
     ({})
  end
end

def self.setBookIsbn(isbn_str)
  if isIsbn(isbn_str)
    db = SQLite3::Database.new(File.dirname(__FILE__) + "/book.db")
    if !isExist(db,isbn_str)
      sql_str = "insert into booklist(isbn) values('#{isbn_str}');"
      db.execute(sql_str)
      true
    else
      false
    end
  else
    false
  end
end

def self.setBookInfo(isbn_str,bookinfo)
  db = SQLite3::Database.new(File.dirname(__FILE__) + "/book.db")
  title = bookinfo["title"] ? bookinfo["title"].gsub(/'/,"''") : ""
  author = ""
  authorlist = bookinfo["authors"]
  if authorlist
    authorlist.each{|a|
      author += a.gsub(/'/,"''") + ","
    }
  end
  author.chop!
  publisher = bookinfo["publisher"] ? bookinfo["publisher"].gsub(/'/,"''") : ""
  publishdate = bookinfo["publishedDate"] ?  bookinfo["publishedDate"].gsub(/'/,"''") : ""
  sql_str = ""
  if isExist(db,isbn_str)
    sql_str = "update booklist set title='#{title}',auther='#{author}',publisher='#{publisher}',publishdate='#{publishdate}' where isbn='#{isbn_str}';"
  else
    sql_str = "insert into booklist values('#{isbn_str}','#{title}','#{author}','#{publisher}','#{publishdate}');"
  end
  db.execute(sql_str)
  true
end

def self.setBookManual(isbn_str, title_str, author_str, publisher_str, publishdate_str)
  if isIsbn(isbn_str)
    title = title_str.gsub(/'/,"''")
    author = author_str.gsub(/'/,"''")
    publisher = publisher_str.gsub(/'/,"''")
    publishdate = publishdate_str.gsub(/'/,"''")
#    sql_str = update booklist set title='#{title}',auther='#{author}',
    sql_str = "update booklist set title='#{title}',auther='#{author}',publisher='#{publisher}',publishdate='#{publishdate}' where isbn='#{isbn_str}';"
#    p sql_str
    db = SQLite3::Database.new(File.dirname(__FILE__) + "/book.db")
    db.execute(sql_str)
    true
  else
    false
  end
end


def self.addBook(isbn_str)
  if isIsbn(isbn_str)
    info = getBookInfo(isbn_str)
    if info.size == 0
      setBookIsbn(isbn_str)
    else
      setBookInfo(isbn_str,info)
    end
  end
end

def self.deleteBook(isbn_str)
  if isIsbn(isbn_str)
    db = SQLite3::Database.new(File.dirname(__FILE__) + "/book.db")
    sql_str = "delete from booklist where isbn='#{isbn_str}';"
    db.execute(sql_str)
    true
  else
    false
  end
end

def self.searchTitle(fragment_str)
  db = SQLite3::Database.new(File.dirname(__FILE__) + "/book.db")
  escaped_str = fragment_str.gsub(/'/,"''")
  sql_str = "select * from booklist where title  like '%#{escaped_str}%' order by publishdate desc;"
  db.execute(sql_str)
end

#分割はrubyで行う
def self.getBookList()
  db = SQLite3::Database.new(File.dirname(__FILE__) + "/book.db")
  sql_str = "select * from booklist order by publishdate desc;"
  db.execute(sql_str)
end

end

#db = SQLite3::Database.new(File.dirname(__FILE__) + "/book.db")
#p Book.isExist(db,'9784873116983')
#p Book.isExist(db,'97848731xx983')
#p Book.searchTitle('')
#p getBookInfo("123456789X")
#p getBookInfo(456)["title"]
#p Book.getBookInfo("4900900648")#["title"]
#Book.addBook("9784873116983")
#require "cgi"
#Book.setBookIsbn("123456789X")
#print "Content-type: text/plain\n\n"

#https = Net::HTTP.new('www.googleapis.com', 443)
#https.use_ssl = true
#https.verify_mode = OpenSSL::SSL::VERIFY_NONE  
#https.verify_depth = 5

#https.start {|w|
#  response = w.get('/books/v1/volumes?q=isbn:4900900648')
#  puts response.body
# json = JSON.parse(response.body)
# p json["items"][0]["volumeInfo"]["title"]
#  json["items"].each do |item|
    #p item["title"]
#    p item
#  end
#}

#uri = URI.parse('https://www.googleapis.com/books/v1/volumes?q=isbn:256')
#json = Net::HTTP.get(uri)
#result = JSON.parse(json)
#puts result

#p json

#f = open("json.txt")
#puts f.read
#j = JSON.parse(f.read)
#p j
#puts "test"
#puts CGI.escape("日本語")
#puts '\n{"日本語":"英語"}'
#f.close

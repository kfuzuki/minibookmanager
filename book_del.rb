#!/usr/bin/ruby
# coding: utf-8

Encoding.default_external = "UTF-8"

require 'json'
require "cgi"
require_relative 'book'

cgi = CGI.new
print "Content-type: text/plain\n\n"

isbn_str = cgi["isbn"]
Book.deleteBook(isbn_str)

list = Book.getBookList()
booklist = {"totalpage" => (list.size/Book::PageSize)+1, "keyword" => "", "page" => 1, "list" => list.take(Book::PageSize)}
print JSON.fast_generate(booklist)

#!/usr/bin/ruby
# coding: utf-8

Encoding.default_external = "UTF-8"

require 'json'
require "cgi"
require_relative 'book'

cgi = CGI.new
print "Content-type: text/plain\n\n"

page = cgi["page"].to_i
field = cgi["field"]
keyword = cgi["word"]

#Book.searchTitle(key)
list = []
if keyword.instance_of?(String) && keyword.length > 0
	list = Book.searchTitle(keyword)
else
	keyword = ""
	list = Book.getBookList()
end
booklist = {"totalpage" => (list.size/Book::PageSize)+1,"keyword" => keyword}
if page > 0
	booklist.store("page",page)
	booklist.store("list",list.drop((page-1)*Book::PageSize).take(Book::PageSize))
else
	booklist.store("page",1)
	booklist.store("list",list.take(Book::PageSize))
end

print JSON.fast_generate(booklist)

#!/usr/bin/ruby
# coding: utf-8

Encoding.default_external = "UTF-8"

require 'json'
require "cgi"
require_relative 'book'

cgi = CGI.new
print "Content-type: text/plain\n\n"

isbn_str = cgi["isbn"]
title_str = cgi["title"]
author_str = cgi["author"]
publisher_str = cgi["publisher"]
publishdate_str = cgi["pdate"]

#Book.addBook(isbn_str)
Book.setBookManual(isbn_str, title_str, author_str, publisher_str, publishdate_str)

list = Book.getBookList()
booklist = {"totalpage" => (list.size/Book::PageSize)+1, "keyword" => "", "page" => 1, "list" => list.take(Book::PageSize)}
print JSON.fast_generate(booklist)
